/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

class MissingDefinitionException extends IllegalDataModelException {
	private static final long serialVersionUID = 3706649203674004259L;

	public MissingDefinitionException(String message) {
		super(message);
	}
}