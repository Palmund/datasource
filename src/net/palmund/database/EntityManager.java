/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import net.palmund.xml.XmlDocument;
import net.palmund.xml.XmlDocumentBuilder;
import net.palmund.xml.XmlNode;

import org.xml.sax.SAXException;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class EntityManager {
	static final String TAG = EntityManager.class.getSimpleName();

	private static Map<Context, EntityManager> managers = new HashMap<Context, EntityManager>();
	
	final int DATABASE_VERSION;
	final String DATABASE_NAME;
	final Context CONTEXT;
	final SQLiteOpenHelperGenerator HELPER_GENERATOR;

	private Map<Class<?>, AbstractEntityManager<?>> implementations = new HashMap<Class<?>, AbstractEntityManager<?>>();
	
	public EntityManager(Context context) {
		try {
			XmlDocument document = new XmlDocumentBuilder().parse(context.getAssets().open("datasource.xml"));
			XmlNode root = document.getRoot();
			XmlNode connectionNode = root.getNamedChild("connection");
			DATABASE_NAME = connectionNode.getNamedChild("database").getValue();
			DATABASE_VERSION = Integer.parseInt(connectionNode.getNamedChild("version").getValue());
			CONTEXT = context;
			HELPER_GENERATOR = new SQLiteOpenHelperGenerator(this);
			return;
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (SAXException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			throw new InitializationException("datasource.xml was not found in \"assets\".");
		}
		throw new InitializationException("Error initializing DataSource.");
	}

	/**
	 * Returns the {@link EntityManager} that is associated with the given
	 * {@link Context}
	 * 
	 * @param context
	 * @return
	 */
	public static EntityManager find(Context context) {
		if (!managers.containsKey(context)) {
			EntityManager manager = new EntityManager(context);
			managers.put(context, manager);
		}
		return managers.get(context);
	}
	
	@SuppressWarnings("unchecked")
	private <T> AbstractEntityManager<T> findImplementation(Class<T> entityClass) {
		Log.i(TAG, "Looking for DataSourceImpl for type "+entityClass.getName());
		if (!implementations.containsKey(entityClass)) {
			Log.i(TAG, "No implementation found... Creating new!");
			SQLiteOpenHelper databaseHelper = HELPER_GENERATOR.createHelperForClass(entityClass);
			AbstractEntityManager<T> implementation = new SQLiteEntityManagerImpl<T>(this, databaseHelper, entityClass);
			implementations.put(entityClass, implementation);
		}
		return (AbstractEntityManager<T>) implementations.get(entityClass);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T create(T entity) {
		AbstractEntityManager<T> impl = (AbstractEntityManager<T>) findImplementation(entity.getClass());
		return impl.create(entity);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T merge(T entity) {
		AbstractEntityManager<T> impl = (AbstractEntityManager<T>) findImplementation(entity.getClass());
		return impl.merge(entity);
	}
	
	public <T> T find(long id, Class<T> entityClass) {
		AbstractEntityManager<T> impl = findImplementation(entityClass);
		return impl.find(id);
	}
	
	public <T> List<T> findAll(Class<T> entityClass) {
		AbstractEntityManager<T> impl = findImplementation(entityClass);
		return impl.findAll();
	}
	
	public String makeCreateTableSQL(Class<?> clazz) {
		return new SQLBuilder().generateCreateTableScriptForClass(clazz);
	}
	
	public <T> void recreate(Class<T> entityClass) {
		AbstractEntityManager<T> impl = findImplementation(entityClass);
		impl.recreate();
	}
}