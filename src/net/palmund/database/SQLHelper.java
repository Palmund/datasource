/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.palmund.database.entity.Column;
import net.palmund.database.entity.Id;
import net.palmund.database.entity.Table;

class SQLHelper {
	public static Column getIdColumn(Class<?> entityClass) {
		Field[] fields = entityClass.getDeclaredFields();
		for (Field field : fields) {
			if (field.getAnnotation(Id.class) != null) {
				return field.getAnnotation(Column.class);
			}
		}
		return null;
	}
	
	public static <A extends Annotation> boolean containsFieldWithAnnotation(Class<?> entityClass, Class<A> annotationClass) {
		Field[] fields = entityClass.getDeclaredFields();
		for (Field field : fields) {
			if (field.getAnnotation(annotationClass) != null) {
				return true;
			}
		}
		return false;
	}
	
	public static <A extends Annotation> boolean isAnnotatedWith(Class<?> entityClass, Class<A> annotationClass) {
		return entityClass.getAnnotation(annotationClass) != null;
	}
	
	public static <A extends Annotation> boolean isAnnotatedWith(Field field, Class<A> annotationClass) {
		return field.getAnnotation(annotationClass) != null;
	}
	
	public static String getTableNameForEntity(Class<?> clazz) {
		Table table = clazz.getAnnotation(Table.class);
		return table.name();
	}
	
	public static String[] getAllColumnsForEntity(Class<?> clazz) {
		Field[] fields = clazz.getDeclaredFields();
		List<String> allColumns = new ArrayList<String>();
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			Column column = field.getAnnotation(Column.class);
			if (column == null) {
				continue;
			}
			allColumns.add(column.name());
		}
		return allColumns.toArray(new String[0]);
	}
}