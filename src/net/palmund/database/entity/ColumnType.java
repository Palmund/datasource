/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database.entity;

public enum ColumnType {
	TEXT("TEXT"),
	INTEGER("INTEGER"),
	FLOAT("REAL"),
	BLOB("BLOB"),
	NULL("NULL"),
	DATE("INTEGER"),
	;
	
	private String description;
	
	private ColumnType(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return getDescription();
	}
}