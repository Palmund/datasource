/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

public class InitializationException extends RuntimeException {
	private static final long serialVersionUID = -1179424476644058453L;

	public InitializationException(String message) {
		super(message);
	}
}