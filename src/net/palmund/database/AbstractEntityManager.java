/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import net.palmund.database.entity.Column;
import net.palmund.database.entity.ForeignKey;
import net.palmund.database.entity.Id;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public abstract class AbstractEntityManager<T> {
	protected enum Transaction {
		CREATING,
		UPDATING,
		;
	}

	static final String TAG = AbstractEntityManager.class.getSimpleName();
	
	protected final String TABLE_NAME;
	protected final String[] ALL_COLUMNS;
	protected final SQLiteOpenHelper databaseHelper;
	protected final Class<T> ENTITY_CLASS;
	protected final EntityManager MANAGER;
	
	private SQLiteDatabase database;
	
	public AbstractEntityManager(EntityManager source, SQLiteOpenHelper databaseHelper, Class<T> entityClass) {
		this.MANAGER = source;
		this.databaseHelper = databaseHelper;
		this.ENTITY_CLASS = entityClass;
		this.TABLE_NAME = SQLHelper.getTableNameForEntity(entityClass);
		this.ALL_COLUMNS = SQLHelper.getAllColumnsForEntity(entityClass);
		testForDatabaseExistence();
	}
	
	private void testForDatabaseExistence() {
		openRead();
		try {
			database.query(TABLE_NAME, ALL_COLUMNS, null, null, null, null, null);
		} catch (SQLiteException e) {
			close();
			openWrite();
			databaseHelper.onCreate(database);
		}
	}
	
	protected final void openRead() {
		database = databaseHelper.getReadableDatabase();
	}
	protected final void openWrite() {
		database = databaseHelper.getWritableDatabase();
	}
	
	protected final void close() {
		database.close();
	}
	
	public SQLiteDatabase getDatabase() {
		return database;
	}
	
	protected ContentValues entityToContentValues(T entity, Class<?> entityClass, Transaction transaction) {
		ContentValues values = new ContentValues();
		Field[] fields = entityClass.getDeclaredFields();
		for (Field field : fields) {
			try {
				Column column = field.getAnnotation(Column.class);
				if (column == null) {
					continue;
				}
				Method getterMethod = getGetterMethod(field, entityClass);
				Object argument = getterMethod.invoke(entity, (Object[])null);
				if (field.isAnnotationPresent(ForeignKey.class)) {
					Log.i(TAG, "Found a foreign key...");
					if (argument == null) {
						argument = null;
					} else {
						argument = handleForeignKeyObjectAndReturnId(argument, transaction);
					}
				}
				Log.i(TAG, "Putting value ["+argument+"] for "+column.name());
				putValue(values, argument, column);
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return values;
	}
	
	protected Method getGetterMethod(Field field, Class<?> entityClass) throws NoSuchMethodException {
		String methodName = generateGetterMethodName(field);
		Method setterMethod = entityClass.getDeclaredMethod(methodName);
		return setterMethod;
	}
	
	private String generateGetterMethodName(Field field) {
		String fullname = field.getName();
		char firstChar = fullname.toCharArray()[0];
		char capitalizedFirstChar = Character.toUpperCase(firstChar);
		return "get"+capitalizedFirstChar + fullname.substring(1);
	}
	
	private <E> long handleForeignKeyObjectAndReturnId(E entity, Transaction transaction) {
		if (transaction == Transaction.CREATING) {
			entity = MANAGER.create(entity);
			Log.i(TAG, "Persisting foreign key...");
		} else {
			entity = MANAGER.merge(entity);
			Log.i(TAG, "Merging foreign key...");
		}
		Field[] fields = entity.getClass().getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(Id.class)) {
				try {
					Method getterMethod = getGetterMethod(field, entity.getClass());
					return (Long) getterMethod.invoke(entity, (Object[])null);
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		return -1;
	}
	
	private <E> void putValue(ContentValues values, E value, Column column) {
		String key = column.name();
		if (value == null) {
			Log.i(TAG, "Value if NULL -> putNull for "+key);
			values.putNull(key);
		} else {
			Class<?> valueClass = value.getClass();
			if (valueClass == Boolean.class || valueClass == boolean.class) {
				values.put(key, (Boolean)value);
			} else if (valueClass == java.util.Date.class) {
				long date = ((java.util.Date)value).getTime();
				values.put(key, date);
			} else if (valueClass  == java.sql.Date.class) {	// DATE
				long date = ((java.sql.Date)value).getTime();
				values.put(key, date);
			} else if (valueClass == Double.class || valueClass == double.class) {
				values.put(key, (Double)value);
			} else if (valueClass == Float.class || valueClass == float.class) {
				values.put(key, (Float)value);
			} else if (valueClass == Integer.class || valueClass == int.class) {
				values.put(key, (Integer)value);
			} else if (valueClass == Long.class || valueClass == long.class) {
				values.put(key, (Long)value);
			} else if (valueClass == Short.class || valueClass == short.class) {
				values.put(key, (Short)value);
			} else if (valueClass == String.class) {
				values.put(key, (String)value);
			}
		}
	}
	
	protected T cursorToEntity(Cursor cursor) throws IllegalDataModelException {
		T object = null;
		try {
			object = ENTITY_CLASS.newInstance();
			Field[] fields = ENTITY_CLASS.getDeclaredFields();
			for (Field field : fields) {
				try {
					if (SQLHelper.isAnnotatedWith(field, Column.class)) {
						Column column = field.getAnnotation(Column.class);
						Class<?> fieldType = field.getType();
						Object argument;
						if (field.isAnnotationPresent(ForeignKey.class)) {
							long foreignKeyId = findForeignKeyId(field, column, cursor);
							argument = MANAGER.find(foreignKeyId, fieldType);
						} else {
							argument = getArgumentForFieldFromCursor(fieldType, column, cursor);
						}
						Method setterMethod = getSetterMethod(field, ENTITY_CLASS);
						setterMethod.invoke(object, argument);
					}
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
					throw new IllegalDataModelException("The class "+ENTITY_CLASS.getSimpleName()+" does not define a setter method for "+field.getName());
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					throw new IllegalDataModelException("The setter method for "+field.getName()+" in type "+ENTITY_CLASS.getSimpleName()+" does not accept an argument of type "+field.getType());
				} catch (InvocationTargetException e) {
					e.printStackTrace();
					throw new IllegalDataModelException("Unknown exception!");
				}
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw new NonArgConstructorNotFound(ENTITY_CLASS);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new NonArgConstructorNotFound(ENTITY_CLASS);
		} catch (CursorIndexOutOfBoundsException e) {
			e.printStackTrace();
			object = null;
		}
		return object;
	}
	
	private long findForeignKeyId(Field field, Column column, Cursor cursor) {
		int columnIndex = cursor.getColumnIndex(column.name());
		return cursor.getLong(columnIndex);
	}

	protected Object getArgumentForFieldFromCursor(Class<?> fieldType, Column column, Cursor cursor) throws CannotHandleDataTypeException {
		int columnIndex = cursor.getColumnIndex(column.name());
		if (fieldType == java.util.Date.class) {		// util.Date
			long date = cursor.getLong(columnIndex);
			return new java.util.Date(date);
		} else if (fieldType == java.sql.Date.class) {	// sql.DATE
			long date = cursor.getLong(columnIndex);
			return new java.sql.Date(date);
		} else if (fieldType == Double.class || fieldType == double.class) {// DOUBLE
			return cursor.getDouble(columnIndex);
		} else if (fieldType == Float.class || fieldType == float.class) {	// FLOAT
			return cursor.getFloat(columnIndex);
		} else if (fieldType == Integer.class || fieldType == int.class) {	// INTEGER
			return cursor.getInt(columnIndex);
		} else if (fieldType == Long.class || fieldType == long.class) {	// LONG
			return cursor.getLong(columnIndex);
		} else if (fieldType == Short.class || fieldType == short.class) {	// SHORT
			return cursor.getShort(columnIndex);
		} else if (fieldType == String.class) {								// STRING
			return cursor.getString(columnIndex);
		}
		throw new CannotHandleDataTypeException("This class should be subclassed in order to handle the type "+fieldType);
	}
	
	protected Method getSetterMethod(Field field, Class<?> entityClass) throws NoSuchMethodException {
		String methodName = generateSetterMethodName(field);
		Method setterMethod = entityClass.getDeclaredMethod(methodName, field.getType());
		return setterMethod;
	}
	
	private String generateSetterMethodName(Field field) {
		String fullname = field.getName();
		char firstChar = fullname.toCharArray()[0];
		char capitalizedFirstChar = Character.toUpperCase(firstChar);
		return "set"+capitalizedFirstChar + fullname.substring(1);
	}
	
	protected Long findEntityIdForMerge(T entity) {
		long insertId = 0;
		Field[] fields = ENTITY_CLASS.getDeclaredFields();
		for (Field field : fields) {
			if (field.getAnnotation(Id.class) != null) {
				try {
					Method getter = getGetterMethod(field, ENTITY_CLASS);
					insertId = (Long) getter.invoke(entity, (Object[])null);
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		return insertId;
	}

	public abstract void recreate();

	public abstract List<T> findAll();

	public abstract T find(long id);

	public abstract T merge(T entity);

	public abstract T create(T entity);
}