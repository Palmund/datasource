/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

class MissingIdColumnDefinition extends MissingDefinitionException {
	private static final long serialVersionUID = 6646358071593349171L;

	MissingIdColumnDefinition(Class<?> clazz) {
		super("The type "+clazz.getName()+" does not define an id column.");
	}
}