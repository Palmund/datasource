/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

public class IllegalDataModelException extends RuntimeException {
	private static final long serialVersionUID = -4986459962032394092L;

	public IllegalDataModelException(String message) {
		super(message);
	}
}