/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

import java.lang.Character.UnicodeBlock;
import java.lang.reflect.Field;

import net.palmund.database.entity.Column;
import net.palmund.database.entity.ForeignKey;
import net.palmund.database.entity.NotNull;
import net.palmund.database.entity.PrimaryKey;
import net.palmund.database.entity.Table;
import net.palmund.database.entity.Unique;

public class SQLBuilder {
	public String generateDropTableScriptForClass(Class<?> clazz) throws IllegalDataModelException {
		if (SQLHelper.isAnnotatedWith(clazz, Table.class)) {
			Table table = clazz.getAnnotation(Table.class);
			return "DROP TABLE IF EXISTS "+table.name();
		}
		throw new NoTableReferenceException(clazz);
	}
	
	public String generateCreateTableScriptForClass(Class<?> clazz) {
		StringBuilder builder = new StringBuilder();
		Table table = clazz.getAnnotation(Table.class);
		builder.append("CREATE TABLE "+table.name()+" (\n");
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			Column column = field.getAnnotation(Column.class);
			if (column == null) {
				continue;
			}
			builder.append(column.name()+" "+column.type());
			applyAnnotations(field, builder);
			builder.append(",\n");
		}
		applyConstraints(fields, builder);
		builder.setLength(builder.length() - 2);
		builder.append(");");
		return builder.toString();
	}
	
	private void applyConstraints(Field[] fields, StringBuilder builder) throws IllegalDataModelException {
		for (Field field : fields) {
			if (SQLHelper.isAnnotatedWith(field, ForeignKey.class)) {
				builder.append("\n");
				ForeignKey foreignKey = field.getAnnotation(ForeignKey.class);
				Class<?> referencedTable = foreignKey.referencedBy();
				if (!SQLHelper.isAnnotatedWith(referencedTable, Table.class)) {
					throw new IllegalDataModelException("The type "+referencedTable.getName()+" is not a valid reference table.");
				}
				Column column = field.getAnnotation(Column.class);
				Table table = referencedTable.getAnnotation(Table.class);
				String referencedTableName = table.name();
				String formattedConstraint = String.format(foreignKey.description(), column.name(), referencedTableName, foreignKey.column());
				builder.append(formattedConstraint+",\n");
			}
		}
	}

	private void applyAnnotations(Field field, StringBuilder builder) {
//		AutoIncrement autoIncrement = field.getAnnotation(AutoIncrement.class);
//		if (autoIncrement != null) {
//			builder.append(" "+autoIncrement.description());
//		}
		NotNull notNull = field.getAnnotation(NotNull.class);
		if (notNull != null) {
			builder.append(" "+notNull.description());
		}
		PrimaryKey primaryKey = field.getAnnotation(PrimaryKey.class);
		if (primaryKey != null) {
			builder.append(" "+primaryKey.description());
		}
		Unique unique = field.getAnnotation(Unique.class);
		if (unique != null) {
			builder.append(" "+unique.description());
		}
	}
}