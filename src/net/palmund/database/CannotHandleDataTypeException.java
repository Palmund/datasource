/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

public class CannotHandleDataTypeException extends RuntimeException {
	private static final long serialVersionUID = -7835948945520397571L;

	public CannotHandleDataTypeException(String message) {
		super(message);
	}
}
