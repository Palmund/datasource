/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

class NonArgConstructorNotFound extends IllegalDataModelException {
	private static final long serialVersionUID = 5776744870569067565L;

	public NonArgConstructorNotFound(Class<?> clazz) {
		super("The class "+clazz.getName()+" does not have a non arg constructor.");
	}
}