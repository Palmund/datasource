/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.palmund.database.entity.Column;
import net.palmund.database.entity.Id;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

class SQLiteEntityManagerImpl<T> extends AbstractEntityManager<T> {
	public SQLiteEntityManagerImpl(EntityManager source, SQLiteOpenHelper databaseHelper, Class<T> entityClass) {
		super(source, databaseHelper, entityClass);
	}
	
	@Override
	public T create(T entity) {
		openWrite();
		if (SQLHelper.containsFieldWithAnnotation(ENTITY_CLASS, Id.class)) {
			ContentValues values = entityToContentValues(entity, ENTITY_CLASS, Transaction.CREATING);
			//Column idColumn = SQLHelper.getIdColumn(ENTITY_CLASS);
			//values.remove(idColumn.name());
			openWrite();
			long insertId = getDatabase().insertOrThrow(TABLE_NAME, null, values);
			close();
			entity = find(insertId);
		}
		return entity;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T merge(T entity) {
		openRead();
		openWrite();
		Class<T> entityClass = (Class<T>) entity.getClass();
		if (SQLHelper.containsFieldWithAnnotation(entityClass, Id.class)) {
			ContentValues values = entityToContentValues(entity, entityClass, Transaction.UPDATING);
			Column idColumn = SQLHelper.getIdColumn(entityClass);
			values.remove(idColumn.name());
			openWrite();
			long insertId = findEntityIdForMerge(entity);
			insertId = getDatabase().update(TABLE_NAME, values, createWhereArgsForId(entityClass, insertId), null);
			Cursor cursor = getDatabase().query(TABLE_NAME, ALL_COLUMNS, createWhereArgsForId(entityClass, insertId), null, null, null, null);
			cursor.moveToFirst();
			entity = cursorToEntity(cursor);
		}
		return entity;
	}
	
	@Override
	public T find(long id) {
		openRead();
		String whereArg = createWhereArgsForId(ENTITY_CLASS, id);
		Cursor cursor = getDatabase().query(TABLE_NAME, ALL_COLUMNS, whereArg, null, null, null, null);
		cursor.moveToFirst();
		close();
		return cursorToEntity(cursor);
	}
	
	private String createWhereArgsForId(Class<T> entityClass, long id) {
		Field[] fields = entityClass.getDeclaredFields();
		for (Field field : fields) {
			if (field.getAnnotation(Id.class) != null) {
				Column column = field.getAnnotation(Column.class);
				return column.name()+"="+id;
			}
		}
		throw new MissingIdColumnDefinition(entityClass);
	}

	protected Object getArgumentForFieldFromCursor(Class<?> fieldType, Column column, Cursor cursor) throws CannotHandleDataTypeException {
		int columnIndex = cursor.getColumnIndex(column.name());
		if (fieldType == java.util.Date.class) {		// util.Date
			long date = cursor.getLong(columnIndex);
			return new java.util.Date(date);
		} else if (fieldType == java.sql.Date.class) {	// sql.DATE
			long date = cursor.getLong(columnIndex);
			return new java.sql.Date(date);
		} else if (fieldType == Double.class || fieldType == double.class) {// DOUBLE
			return cursor.getDouble(columnIndex);
		} else if (fieldType == Float.class || fieldType == float.class) {	// FLOAT
			return cursor.getFloat(columnIndex);
		} else if (fieldType == Integer.class || fieldType == int.class) {	// INTEGER
			return cursor.getInt(columnIndex);
		} else if (fieldType == Long.class || fieldType == long.class) {	// LONG
			return cursor.getLong(columnIndex);
		} else if (fieldType == Short.class || fieldType == short.class) {	// SHORT
			return cursor.getShort(columnIndex);
		} else if (fieldType == String.class) {								// STRING
			return cursor.getString(columnIndex);
		}
		throw new CannotHandleDataTypeException("This class should be subclassed in order to handle the type "+fieldType);
	}

	@Override
	public List<T> findAll() {
		openRead();
		Cursor cursor = getDatabase().query(TABLE_NAME, ALL_COLUMNS, null, null, null, null, null);
		System.out.println(cursor.getCount());
		cursor.moveToFirst();
		List<T> allEntities = new ArrayList<T>();
		while (!cursor.isAfterLast()) {
			T entity = cursorToEntity(cursor);
			allEntities.add(entity);
			cursor.moveToNext();
		}
		close();
		return allEntities;
	}
	
	@Override
	public void recreate() {
		openWrite();
		SQLBuilder builder = new SQLBuilder();
		String dropScript = builder.generateDropTableScriptForClass(ENTITY_CLASS);
		String createScript = builder.generateCreateTableScriptForClass(ENTITY_CLASS);
		getDatabase().execSQL(dropScript);
		getDatabase().execSQL(createScript);
	}
}