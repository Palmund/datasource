/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

class NoTableReferenceException extends IllegalDataModelException {
	private static final long serialVersionUID = 6731485692545342021L;

	public NoTableReferenceException(Class<?> clazz) {
		super("The type "+clazz.getName()+" does not define a table reference.");
	}
}