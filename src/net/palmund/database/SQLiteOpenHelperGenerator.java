/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

import net.palmund.database.entity.Table;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class SQLiteOpenHelperGenerator {
	private final EntityManager generator;

	protected SQLiteOpenHelperGenerator(EntityManager dataSource) {
		generator = dataSource;
	}

	public SQLiteOpenHelper createHelperForClass(Class<?> clazz) throws IllegalDataModelException {
		if (SQLHelper.isAnnotatedWith(clazz, Table.class)) {
			final String TABLE_NAME = getTableNameForClass(clazz);
			final String DATABASE_CREATE = new SQLBuilder().generateCreateTableScriptForClass(clazz);
			return new SQLiteOpenHelper(generator.CONTEXT, generator.DATABASE_NAME, null, generator.DATABASE_VERSION) {
				@Override
				public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
					Log.w(this.getClass().getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
					db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
					onCreate(db);
				}
				
				@Override
				public void onCreate(SQLiteDatabase db) {
					db.execSQL(DATABASE_CREATE);
				}
			};
		}
		throw new NoTableReferenceException(clazz); //"The type "+clazz.getName()+" does not define a table reference.");
	}
	
	private String getTableNameForClass(Class<?> clazz) {
		Table table = clazz.getAnnotation(Table.class);
		return table.name();
	}
}