/*
 * Copyright 2013 (c) S�ren Palmund
 * 
 * Licensed under the License described in LICENSE (the "License"); you may not
 * use this file except in compliance with the License.
 */

package net.palmund.database;

enum PropertyKey {
	VERSION("datasource.version"),
	NAME("datasource.database"),
	PROPERTY_FILE("datasource.properties"),
	;
	
	private String keyPath;
	
	private PropertyKey(String keyPath) {
		this.keyPath = keyPath;
	}
	
	public String getKeyPath() {
		return keyPath;
	}
}